//
//  testbugTests.m
//  testbugTests
//
//  Created by Daniel Phillips on 07/08/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface testbugTests : XCTestCase

@end

@implementation testbugTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
