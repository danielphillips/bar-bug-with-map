//
//  MTMixedTableViewController.m
//  testbug
//
//  Created by Daniel Phillips on 07/08/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import "MTMixedTableViewController.h"
#import "MTMapViewCell.h"
@import MapKit;

@interface MTMixedTableViewController ()<MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) MKMapView *mapView;
@property (nonatomic, strong) UITableViewCell *mapViewCell;

@end

@implementation MTMixedTableViewController
static NSString *cellIdentifier = @"MTCellIdentifier";

- (void)loadView {
    [super loadView];

    [self mapViewCell];
}

- (MKMapView *)mapView {
    if (!self->_mapView) {
        MKMapView *mapView = [[MKMapView alloc] initWithFrame:CGRectZero];
        self->_mapView = mapView;
    }
    return self->_mapView;
}

- (UITableViewCell *)mapViewCell {
    if (!self->_mapViewCell) {
        MTMapViewCell *cell = [[MTMapViewCell alloc] init];
        cell.mapView = self.mapView;
        self->_mapViewCell = cell;
    }
    return self->_mapViewCell;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 0 ? 300.0f : 70.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0 && tableView == self.tableView)
        return 1;
    else
        return 20.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0)
        return self.mapViewCell;
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = @"Some data.";
    
    return cell;
}


@end
