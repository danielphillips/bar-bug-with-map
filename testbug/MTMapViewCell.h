//
//  MTMapViewCell.h
//  testbug
//
//  Created by Daniel Phillips on 10/06/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

@class MKMapView;

@interface MTMapViewCell : UITableViewCell

@property (nonatomic, strong) MKMapView *mapView;

@end
