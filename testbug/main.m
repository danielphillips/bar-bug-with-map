//
//  main.m
//  testbug
//
//  Created by Daniel Phillips on 07/08/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MTAppDelegate class]));
    }
}
