//
//  MTMapViewCell.m
//  testbug
//
//  Created by Daniel Phillips on 10/06/2013.
//  Copyright (c) 2013 Daniel Phillips. All rights reserved.
//

#import "MTMapViewCell.h"
@import MapKit;

@implementation MTMapViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    if (!self.mapView.superview){
        [self addSubview:self.mapView];
        self.mapView.frame = self.contentView.bounds;
        self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
}

@end
